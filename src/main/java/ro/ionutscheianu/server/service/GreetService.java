package ro.ionutscheianu.server.service;


import io.grpc.stub.StreamObserver;
import ro.ionutscheianu.generated.EmptyResponseOuterClass;
import ro.ionutscheianu.generated.GreetRequestOuterClass;
import ro.ionutscheianu.generated.GreetServiceGrpc;

public class GreetService extends GreetServiceGrpc.GreetServiceImplBase {
    @Override
    public void greet(GreetRequestOuterClass.GreetRequest request, StreamObserver<EmptyResponseOuterClass.EmptyResponse> responseObserver) {
        System.out.println(request.getName());
        EmptyResponseOuterClass.EmptyResponse.Builder emptyResponseBuilder = EmptyResponseOuterClass.EmptyResponse.newBuilder();
        responseObserver.onNext(emptyResponseBuilder.build());
        responseObserver.onCompleted();
    }
}