package ro.ionutscheianu.client;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import ro.ionutscheianu.generated.GreetRequestOuterClass;
import ro.ionutscheianu.generated.GreetServiceGrpc;
import ro.ionutscheianu.utils.Utils;

import java.util.Properties;
import java.util.Scanner;

public class Client {
    private ManagedChannel managedChannel;

    public Client() {
        Properties properties = Utils.loadProperties();
        managedChannel = ManagedChannelBuilder.forAddress(properties.getProperty("ip"), Integer.parseInt(properties.getProperty("port"))).usePlaintext().build();
    }

    public void start() {
        String name;
        System.out.println("The server wants to know your name!");
        Scanner scanner = new Scanner(System.in);
        name = scanner.nextLine();
        sendName(name);
    }

    private void sendName(String name) {
        GreetServiceGrpc.GreetServiceBlockingStub greetServiceBlockingStub = GreetServiceGrpc.newBlockingStub(managedChannel);
        GreetRequestOuterClass.GreetRequest greetRequest = GreetRequestOuterClass.GreetRequest
                .newBuilder()
                .setName(name)
                .build();
        greetServiceBlockingStub.greet(greetRequest);
    }

}
