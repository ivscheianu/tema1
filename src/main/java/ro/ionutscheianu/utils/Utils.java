package ro.ionutscheianu.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class Utils {

    private Utils() {

    }

    public static Properties loadProperties() {
        Properties properties = null;
        try (InputStream input = new FileInputStream("src/main/resources/properties/config.properties")) {
            properties = new Properties();
            properties.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        if (properties == null) {
            throw new NullPointerException();
        }
        return properties;
    }
}